<?php

return[

	'form_pet_nom_site2' => 'Votre code postal',
	'form_pet_mail_confirmation' => 'Bonjour,

Vous avez demandé à signer la pétition :
@titre@.

Vous avez fourni les informations suivantes :
    Nom : @nom_email@
    Code postal: @nom_site@

IMPORTANT...
Pour valider votre signature, il suffit de vous connecter à
l’adresse ci-dessous (dans le cas contraire, votre demande
sera rejetée) :

    @url@


Merci de votre participation
',

];
