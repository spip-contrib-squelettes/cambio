<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_importer_signatures_charger_dist($id_petition) {
	if (autoriser('configurer')) {
		return array('file_import' => '');
	} else {
		return false;
	}
}

function formulaires_importer_signatures_verifier_dist($id_petition) {
	$erreurs = array();

	if (!isset($_FILES) or !isset($_FILES['file_import'])) {
		$erreurs['file_import'] = _T('medias:erreur_indiquez_un_fichier');
	}

	return $erreurs;
}

function formulaires_importer_signatures_traiter_dist($id_petition) {

	$importer_csv = charger_fonction('importer_csv', 'inc');

	if (isset($_FILES) and isset($_FILES['file_import']) and !$_FILES['file_import']['error']) {
		$fichier = $_FILES['file_import']['tmp_name'];
		$csv = $importer_csv($fichier, true, ",", '"', null);
		$petition = sql_fetsel('*', 'spip_petitions', 'id_petition=' . intval($id_petition));
		$total = 0;
		include_spip('action/editer_signature');
		if (is_array($csv) and count($csv) >= 1) {
			foreach ($csv as $item) {
				$doublon = false;
				if ($petition['email_unique'] == "oui") {
					$doublon = sql_countsel('spip_signatures', "id_petition=" . intval($id_petition) . " AND ad_email=" . sql_quote($item['email']) . " AND statut='publie'");
				}
				if (!$doublon) {
					if ($id_signature = signature_inserer($id_petition)) {
						signature_modifier(
							$id_signature,
							array(
								'statut'    => 'publie',
								'nom_email' => $item['nom'],
								'ad_email'  => $item['email'],
								'nom_site'  => $item['code_postal']
							)
						);
						++$total;
					}
				}
			}
		}
	}

	return array(
		'message_ok' => _T('cambio:import_message_ok', array('nb' => $total)),
		'editable'   => true
	);
}
