# Cambio

*parce qu'il est temps de changer le monde...*

Cambio est un squelette SPIP qui permet de mettre en place facilement un site de pétitions.

Voir l'instance d'Eau & rivières de Bretagne pour une démonstration : https://petitions.eau-et-rivieres.org/

## Fonctionnement

Tout d'abord, activez les logos de survol et le champ Chapeau des articles dans la page de configuration du contenu du site.

Le contenu de la page d'accueil est défini dans un article page unique portant l'identifiant `home`. Elle affiche dans l'ordre : le logo du site, le titre de l'article suivi de son chapeau, la liste des pétitions en cours, puis le texte de l'article. Le logo de l'article est affiché en fond de page.

Par défaut, il faut créer une rubrique qui contiendra les articles des pétitions. Chaque article publié dans celle-ci sera affiché en page d'accueil dans une liste des pétitions en cours.

Le contenu d'une page de pétition est donc défini par l'article qui la porte. La page affiche dans l'ordre : le logo de survol de l'article, le titre suivi du chapeau, le formulaire pour signer la pétition, puis le texte de l'article. Le logo de l'article est affiché en fond de page.

Il possible d'afficher un item supplémentaire en tête des éléments du pied de page en renseignant le contenu du champ "Slogan du site" depuis la page de configuration "Identité du site". Vous pouvez y utiliser la syntaxe des liens de SPIP, ceux-ci seront interprétés par le squelette.

## Crédits
- Template graphique par [HTML5 UP](https://html5up.net/demos/identity)
- Logo par [Remix Icon](https://remixicon.com/)
